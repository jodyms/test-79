import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-case',
  templateUrl: './case.component.html',
  styleUrls: ['./case.component.css']
})
export class CaseComponent implements OnInit {

  @Input('name')
  public name;
  private hidden;

  @Output('afterNameInitiated')
  public afterNameInitiated: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
  }

  public ngOnInit() {
    this.hidden = false;
    if (this.name) {
      this.afterNameInitiated.emit(true);
    }
  }

  public isHidden() {
    return this.hidden;
  }

  public hide() {
    this.hidden = true;
  }

}

import { Component, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  rows = [];
  public load = false;
  public username = [];
  temp = this.rows;
  public iduser;
  public titleval;
  public button: number;
  public httpOptions;
  public todoform;
  public listTodo: any[] = [];
  public api = 'https://jsonplaceholder.typicode.com/';
  public click = true;

  constructor(private fb: FormBuilder, private http: HttpClient) {
    // Tambahkan validasi untuk field 'title' dengan validasi 'Required'
      this.todoform = this.fb.group({
      id: null,
      userId: [' ', Validators.required],
      title: [' ', Validators.required],
      body: ''
    });
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
  }

  public ngOnInit() {
    this.button = 1;
    this.onLoad();
  }

  public onSubmit() {
    this.http.post( this.api + 'posts', this.todoform.value, this.httpOptions ).subscribe(
      (success: any) => {
        this.listTodo.push(success);
        console.log(this.listTodo);
        this.reset();
      }
    );
    // Gunakan http.POST (Untuk Tambah Baru) & http.PUT (Untuk Ubah Data) dari Restful API yang didefinisikan pada variable api
  }

  public reset() {
    this.button = 1;
    this.todoform.patchValue({ userId : '' });
    this.todoform.patchValue({ title : '' });
    this.todoform.patchValue({ body : '' });
    this.todoform.patchValue({ id : '' });
  }

  public onLoad() {
    this.http.get( this.api + 'posts', this.httpOptions).subscribe(
      (success: any) => {
        this.temp = success;
        this.rows = this.temp;
        console.log(success);
      }
    );
    // Gunakan http.GET (Untuk mengambil data untuk table)
  }

  public onRemove(id) {
    this.http.delete( this.api + 'posts/' + id, this.httpOptions).subscribe(
      (success: any) => {
        console.log(success);
      }
    );
    // Gunakan http.DELETE (Untuk menghapus data untuk table)
  }

  public update(id) {
    this.button =  2;
    this.http.get( this.api + 'posts/' + id, this.httpOptions).subscribe(
      (success: any) => {
        console.log(success);
        this.todoform.patchValue({ userId : success.userId });
        this.todoform.patchValue({ title : success.title});
        this.todoform.patchValue({ body : success.body});
        this.todoform.patchValue({ id : success.id});
        this.reset();
      }
    );
  }
  public ambilData() {
    this.click = true;
    if ( this.username != null) {
      this.load = false;
    }
    this.load = false;
    this.http.get(this.api + 'users/' + this.iduser).subscribe(
      (success: any) => {
        this.username = [];
        this.username.push(success);
        console.log(this.username);
        this.load = false;

      },
      err => {
        this.username = [];
      }
    );
  }

  public setIdUser(id) {
    this.click = false;
    console.log('ter');
    this.load = false;
    this.todoform.patchValue({ userId : id});
  }

  public onUpdate(id) {
    this.http.put( this.api + 'posts/' + id, this.todoform.value, this.httpOptions).subscribe(
      (success: any) => {
        console.log(success);
        this.reset();
      }
    );
  }
}
